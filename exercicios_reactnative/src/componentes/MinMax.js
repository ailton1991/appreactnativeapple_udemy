import React from 'react'
import { Text } from 'react-native'
import Estilo from './estilo'

export default (props) => { //Convenção props = propriedades
    console.warn(props);
    return (

                <Text style={Estilo.txtGrande}> 

                    Valor {props.max} X é maior do que valor {props.min}... 
                   
                    Total {props.max + props.min}

                </Text>

           )}