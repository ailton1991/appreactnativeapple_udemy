import React, {useState} from "react";
import { Text, Button } from 'react-native'
import Estilo from './estilo'

export default ({inicial = 0, passo = 10}) => {
//export default props => {

    //let numero = props.inicial;
    //let numero = inicial;
    const [numero, setNumero] = useState(inicial, passo)

    const inc = () => { setNumero(numero + passo) };
    const dec = () => setNumero(numero - passo);
    const zerar = () => { setNumero(0) }

    return (

        <>
            <Text style={Estilo.txtGrande}> {numero} </Text>
            <Button title="+" onPress={inc} />
            <Button title="-" onPress={dec} />
            <Button title="Zerar" onPress={zerar}/>
        </>

    )

}