function Produto (nome) {
    this.nome = nome;
}


const p1 = new Produto('Caneta');
const p2 = new Produto('Caderno');


console.log("Ailton Xavier");
console.log(p1.nome, p2.nome);
console.log(typeof Produto);
console.log(typeof p1);
console.log(p2);
console.log(typeof p1.nome);
console.log(typeof p2.nome);


