import React from 'react'
import { View, SafeAreaView, StyleSheet, Image } from 'react-native'

import CompOficial, { Comp1, Comp2 } from './componentes/Multi'
import Primeiro from './componentes/Primeiro'
import MinMax from './componentes/MinMax'
import Aleatorio from './componentes/Aleatorio'
import Titulo from './componentes/Titulo'
import Botao from './componentes/Botao'
import Contador from './componentes/Contador';
import PaiDireta from './componentes/direta/Pai'
import PaiIndireta from './componentes/indireta/Pai'
import ContadorV2 from './componentes/contador/ContadorV2'
import Diferenciar from './componentes/diferenciar_android_ios/Diferenciar'
import ParImpar from './componentes/ParImpar'
import Familia from './componentes/relacao/Familia'
import Membro from './componentes/relacao/Membro'
import UsuarioLogado from './componentes/relacao_direta_2/UsuarioLogado'
import TimeDeTorcida from './componentes/relacao_direta_2/TimeDeTorcida'
import ExibirImagem from './componentes/Imagem/exibirImagem'
import ListarProdutos from './componentes/produtos/ListarProdutos'
import ListarCombustivel from './componentes/produtos/ListarCombustivel'

export default () =>  (
    <SafeAreaView style={style.App}>
        {/* <View style={style.App}> */}
            
             {/*<CompOficial />
            <Comp1 />
            <Comp2 />
            <Primeiro />*/}

            {/* <MinMax min="3" max="20" />
            <MinMax min={1} max={94} /> */}
            
            {/* <Primeiro /> */}
            
            {/* <Aleatorio min={1} max={60} />
            <Aleatorio min={1} max={60} />
            <Aleatorio min={1} max={60} />
            <Aleatorio min={1} max={60} />
            <Aleatorio min={1} max={60} />
            <Aleatorio min={1} max={60} /> */}

            {/* <Titulo principal="Novo Cadastro" secundario="Tela de Cadastro do Produto" observacao="É preciso preencher todo o formulário" /> */}
            
            <Botao />

            {/* <Contador inicial={100} passo={50}/> */}

            {/* <Contador passo={1} /> */}

            {/* <PaiDireta /> */}
            
            {/* <PaiIndireta /> */}

            <ContadorV2 />
            
            <Diferenciar />
            
            {/* <ParImpar num={1} /> */}
            
            {/* <Familia>
                <Membro nome={'Ailton'} sobrenome={'Xavier'} />
                <Membro nome={'Ronilda'} sobrenome={'Dias'}/>
            </Familia>

            <Familia>
                <Membro nome={'Camila'} sobrenome={'Oliveira'} />
                <Membro nome={'João'} sobrenome={'Pedro'} />
            </Familia> */}

            {/* <UsuarioLogado usuario={ {nome : 'Ailton', email : 'ailton@gmail.com'} } />
            <UsuarioLogado usuario={null} />
            <UsuarioLogado usuario={{}} />
            <UsuarioLogado usuario={ {nome : 'Hercules', email : 'hercules@gmail.com'} } /> */}

            {/* <UsuarioLogado /> */}

            {/* <TimeDeTorcida time={{ nome : 'Flamengo' }} />
            <TimeDeTorcida time={{ nome : 'Real Madrid' }} /> */}

            <ExibirImagem />

            {/* <ListarProdutos /> */}

            {/* <ListarCombustivel /> */}

        {/* </View> */}
    </SafeAreaView>
)

const style = StyleSheet.create({ 
    App : {
         flexGrow : 1,
         justifyContent : 'center',
         alignItems : 'center',
         alignContent: 'center',
         textAlign : 'center',
         padding: 100,
         height : '100%',
         backgroundColor : '#C4FAFD',
    }
})
