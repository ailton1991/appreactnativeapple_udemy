import React from 'react';
import { SafeAreaView, View, Text } from 'react-native';
import estilo from './estilo';

export default props => {

    return (

        <View>
            <Text style={estilo.txtGrande}>
                {props.principal}
            </Text>
            <Text style={estilo.txtMedio}>
                {props.secundario}
            </Text>
            <Text>
                {props.observacao}
            </Text>
        </View>

        // OU
        //<> Envolvendo o Fragemento, isso pode ser usado no lugar da View
          //  <Text style={estilo.txtGrande}>
            //    {props.principal}
            //</Text>
            //<Text style={estilo.txtMedio}>
                //{props.secundario}
            //</Text>
        //</>

        // OU
        //<React.Fragment> Envolvendo o Fragemento, isso pode ser usado no lugar da View
          //  <Text style={estilo.txtGrande}>
            //    {props.principal}
            //</Text>
            //<Text style={estilo.txtMedio}>
                //{props.secundario}
            //</Text>
        //</React.Fragment>

        // OU
        //import { Fragment } from 'react';
        //<Fragment> Envolvendo o Fragemento, isso pode ser usado no lugar da View
          //  <Text style={estilo.txtGrande}>
            //    {props.principal}
            //</Text>
            //<Text style={estilo.txtMedio}>
                //{props.secundario}
            //</Text>
        //</Fragment>

    )

}