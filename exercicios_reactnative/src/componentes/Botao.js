import React from 'react';
import { View, Button } from 'react-native';

export default props => {

    function executarDiretoBotao(propriedade){
        return function(){
            console.warn("Executado !!!" + propriedade)
        }
    }

    function executar(){
        console.warn("Executado !!! #01")
    }

    return (

        //<Button  title="Executar!"  onPress={executarDiretoBotao(" Cheguei Aqui ")} />
        <>
            
            <Button  title="Executar #01"  onPress={executar} />   
            <Button  title="Executar #02"  onPress={function() { console.warn("Executado #02") }} />   
            <Button  title="Executar #03"  onPress={() => console.warn("Executado #03")} />   
        </>
    )

}