import React from 'react'
import { Text } from 'react-native'
import Estilo from '../estilo'
import IF from './If'

export default props => {
// export default ({ usuario = {} }) => {
    
    let usuario = props.usuario || {}

    let exiteUsuario = Object.keys(usuario).length > 0;

    console.warn(Object.keys(usuario).length)
    console.warn(exiteUsuario)
    console.warn(usuario)
    if(exiteUsuario){
    console.warn(usuario.nome)
    console.warn(usuario.email)
    }

    return (
        <>
            <IF teste={usuario && usuario.nome && usuario.email}>
                <Text style={Estilo.txtGrande}>
                    Usuário Logado é :
                </Text>

                <Text style={Estilo.txtMedio}>
                    {usuario.nome} - {usuario.email}
                </Text>
                <Text />
            </IF>
        </>
    )
}