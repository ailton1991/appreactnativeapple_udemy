import React from 'react'
import { Text } from 'react-native'
import Estilo from '../estilo'
import { useState } from 'react';

import ContadorDisplay from './ContadorDisplay';
import ContadorBotoes from './ContadorBotoes';

export default props => {

    const [num, setNum] = useState(0)

    const inc = () => setNum(num + 1)
    const dec = () => setNum(num - 1)

    return (
        <>
            <Text style={Estilo.txtGrande}> Contador </Text>
            <ContadorDisplay num={num}/>
            <ContadorBotoes inc={inc} dec={dec} />
            <Text style={[Estilo.txtGrande, {color : '#F94304', fontWeight : 'bold'}]}> { num % 2 === 0 ? "Par" : "Ímpar" } </Text>
            <Text></Text>
        </>
    )
}