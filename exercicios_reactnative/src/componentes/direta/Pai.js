import React from 'react';
import { Text } from 'react-native';
import Estilo from '../estilo';
import Filho from './Filho';


// Relação de Pai para Filho, quando um componente acima da árvore de componente passando  para a mais baixa
// Relaçao Direta 
export default props => {
    let x = 25;
    let y = 100;
    return (
        <>
            <Filho a={x} b={y} />
            <Filho a={x + 100} b={y + 200} />
        </>
    )
}