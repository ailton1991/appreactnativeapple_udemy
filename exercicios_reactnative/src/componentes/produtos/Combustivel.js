export default [
    {id : 1, combustivel : "Gasolina Comum", preco : 7.992},
    {id : 2, combustivel : "Gasolina Aditivada", preco : 8.992},
    {id : 3, combustivel : "Gasolina Premmium", preco : 9.992},
    {id : 4, combustivel : "Etanol", preco : 5.692},
    {id : 5, combustivel : "Diesel", preco : 5.792},
]