function useState(inicial){
    return [inicial, function() {
        console.log('chamou! ' + inicial)
    }]
}

const array = useState(10)
console.log(array[0] + ' Console 1 ')

console.log(array[1]() + ' Console 2 ')


const [valor, setValor] = useState('teste3')
console.log(valor + ' console 3 ');
console.log(setValor());
