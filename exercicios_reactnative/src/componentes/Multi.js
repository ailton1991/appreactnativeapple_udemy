import React from 'react';
import { Text } from 'react-native';
import Estilo from './estilo';

export default function () {
    return <Text style={Estilo.txtMedio}>--Componente #Oficial Blz </Text>
}

export function Comp1() {
    return <Text style={Estilo.txtMedio}> Componente #01 </Text>
}

export function Comp2() {
    return <Text style={Estilo.txtMedio}> Componente #02 </Text>
}


//export { Comp1, Comp2 }
