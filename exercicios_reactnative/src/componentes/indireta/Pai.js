import React, { useState } from 'react'
import { Text } from 'react-native'
import Filho from './Filho'
import Estilo from '../estilo'

export default props => {
    
    const [num, setNum] = useState(0);
    const [texto, setTexto] = useState('');
    const [subTexto, setSubTexto] = useState('');

    function exibirValorDoFilho(numero, texto, subTexto){
        setNum(numero)
        setTexto(texto)
        setSubTexto(subTexto)
    }
    
    return (
        <>
            <Text style={Estilo.txtGrande}>{texto}{num}</Text>
            <Filho 
                min={1}
                max={60}
                funcaoDoPai={exibirValorDoFilho}
            />
        </>
    )
}