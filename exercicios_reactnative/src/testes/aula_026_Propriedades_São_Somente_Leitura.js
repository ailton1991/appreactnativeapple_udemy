// Propriedades são somente leituras.
//1º - Não altera o valor da propriedade
let props = Object.freeze({min: 1, max:60});
console.log(props);

//2º - 
console.log(props.min += 1000);

//3º - Mesmo que mude o valor, feito na 2ª etapa não vai mudar a propriedade original.
console.log(props.min);

//4º - Só consegue alterar o valor se, criar uma variável.
let { min } = props //Desconstruir - Destruction ler algo dentro de um objeto.// Atributos dentro do Objeto 
min += 1000
console.log(min)
