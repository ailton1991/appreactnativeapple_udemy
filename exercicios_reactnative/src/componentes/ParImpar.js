import React from 'react'
import { View , Text } from 'react-native'
import Estilo from './estilo'

export default ({num = 0}) => {

    // if(num % 2 === 0){
    //     return <Text style={Estilo.txtGrande}> Número é Par </Text>
    // } else {
    //     return <Text style={Estilo.txtGrande}> Número é Ímpar </Text>
    // }

    // Ou
    // Redirecionamento Condicional, usando IF ternário

    return (
        <View>
            <Text style={Estilo.txtGrande}> O resultado {num} é : </Text>
            <Text style={[Estilo.txtMedio, {fontWeight : 'bold', color : '#69017C'} ]}> { num % 2 === 0 ? "Par" : "Ímpar" } </Text>        
            <Text></Text>
        </View>
    )

}