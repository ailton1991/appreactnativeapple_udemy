import React from 'react'
import { Text } from 'react-native'
import Estilo from '../estilo'
import QualTime from './QualTime'

export default ( { time = {} } ) => {
    
    return (

        <>
            <QualTime time={time && time.nome}>
                <Text style={Estilo.txtGrande}> Meu time é : </Text>
                <Text style={Estilo.txtMedio}> {time.nome} </Text>
                <Text />
            </QualTime>
        </>

    )
}