import React from 'react'
import { Text } from 'react-native'
import Estilo from '../estilo'
import Combustivel from './Combustivel'

export default props => {
    function obterCombustivel(){
        return (
            Combustivel.map(x => {
                return <Text key={x.id}> {x.id}) {x.combustivel} {x.preco} </Text>
            })
        )
    }

    return (
        <>
            <Text>-------------------</Text>
            <Text style={Estilo.txtGrande}> Combustivel : </Text>
            <Text> { Combustivel.map( x =>
                {
                    return ( <Text key={x.id}> {x.id}) {x.combustivel} {'=>'} {x.preco} </Text> )        
                } 
            )} 
            </Text>
            <Text>-------------------</Text>
            <Text>{obterCombustivel()}</Text>
        </>
    )
}