import React from 'react'
import { Text } from 'react-native'
import Estilo from '../estilo'
import Produtos from './Produtos'

export default props => {
    
    function obterProdutos(){
        return Produtos.map( p => { return (
                <Text key={p.id}> {p.id}) {p.produto} com o valor {p.preco} </Text>
            ) }
        )
    }
    
    return (
        <>
            <Text>-------------------</Text>
            <Text style={Estilo.txtGrande}> Lista de Produtos </Text>
            <Text style={Estilo.txtMedio}> { Produtos.map( p => { 
                return ( <Text key={p.id}> {p.id}) {p.produto} = {p.preco} </Text> ) }) }
            </Text>
            <Text>-------------------</Text>
            <Text>{obterProdutos()}</Text>
        </>
    )
}