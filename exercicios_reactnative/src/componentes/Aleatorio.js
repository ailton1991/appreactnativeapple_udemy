import React from 'react'
import { Text } from 'react-native'
import Estilo from './estilo'

//esport default ({min, max}) -- Pode ser dessa maneira também //Desconstruir - Destruction ler algo dentro de um objeto.// Atributos dentro do Objeto 
export default (props) => {

    const {min, max} = props; //Desconstruir - Destruction ler algo dentro de um objeto.// Atributos dentro do Objeto 
    //console.warn(Math.random());
    const delta = max - min + 1;
    const aleatorio = parseInt(Math.random() * delta) + props.min;

    return ( 

        <Text style={Estilo.txtMedio}> 
        
            O Valor aleatório é {aleatorio}

        </Text>

     )

}
